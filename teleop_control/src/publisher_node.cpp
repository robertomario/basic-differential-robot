/**
 * Define node
 * that detects keys being pressed
 * and sends messages accordingly
 */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "teleop_control/keyboard_publisher.h"

void DisplayHelp()
{
    ROS_INFO("This node sends a Twist message in response to keyboard input.\n Available commands:\n"
             "i -> Move forward\n"
             ", -> Move backward\n"
             "k -> Rotate left\n"
             "l -> Rotate right\n"
             "u o m . -> Diagonals\n"
             "w -> Increase linear speed\n"
             "x -> Decrease linear speeed\n"
             "e -> Increase angular speed\n"
             "c -> Decrease angular speed\n"
             "q -> Exit");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "publisher_demo_node");
    ros::NodeHandle n;
    KeyboardPublisher keyboard_publisher(n);
    ros::Rate loop_rate(100);
    // Always start by displaying help message
    DisplayHelp();
    while (ros::ok())
    {
        int message_status = keyboard_publisher.ParseKeyboard();
        if (message_status == -1)
        {
            break;
        }
        else
        {
            if (message_status == 1)
            {
                keyboard_publisher.Publish();
            }
        }
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}