/**
 * Define node
 * that receives message with key pressed
 * and reacts accordingly
 */

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

void demoCallback(const geometry_msgs::Twist msg)
{
    float linear_command = msg.linear.x;
    float angular_command = msg.angular.z;
    ROS_INFO("I heard: [%f] [%f]", linear_command, angular_command);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscriber_demo_node");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("demo_topic", 1000, demoCallback);
    ros::spin();
    return 0;
}