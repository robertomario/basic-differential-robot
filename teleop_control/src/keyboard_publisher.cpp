#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <string>
#include "teleop_control/keyboard_publisher.h"
#include <array>

KeyboardPublisher::KeyboardPublisher(ros::NodeHandle &n) : linear_speed_(10.0), angular_speed_(5.0), speed_change_(0.5)
{
    chatter_pub_ = n.advertise<geometry_msgs::Twist>("demo_topic", 1000);
    reaction_map_['u'] = {1, 1, 1, 0, 0};
    reaction_map_['i'] = {1, 1, 0, 0, 0};
    reaction_map_['o'] = {1, 1, -1, 0, 0};
    reaction_map_['j'] = {1, 0, 1, 0, 0};
    reaction_map_['k'] = {1, 0, 0, 0, 0};
    reaction_map_['l'] = {1, 0, -1, 0, 0};
    reaction_map_['m'] = {1, -1, 1, 0, 0};
    reaction_map_[','] = {1, -1, 0, 0, 0};
    reaction_map_['.'] = {1, -1, -1, 0, 0};
    reaction_map_['w'] = {0, 0, 0, 1, 0};
    reaction_map_['x'] = {0, 0, 0, -1, 0};
    reaction_map_['e'] = {0, 0, 0, 0, 1};
    reaction_map_['c'] = {0, 0, 0, 0, -1};
    ResetMessage();
}

KeyboardPublisher::~KeyboardPublisher()
{
}

int KeyboardPublisher::ParseKeyboard()
{
    char c = Getch();
    ROS_INFO("%c", c);
    int is_message_ready;
    ResetMessage();
    if (c == 'q' || c == '\x03')
    {
        ROS_INFO("Exiting upon user request");
        is_message_ready = -1;
    }
    else
    {
        if (reaction_map_.count(c) > 0)
        {
            std::array<int, 5> command = reaction_map_[c];
            is_message_ready = command.at(0);
            msg_.linear.x += command.at(1) * linear_speed_;
            msg_.angular.z += command.at(2) * angular_speed_;
            if (command.at(3) != 0)
            {
                linear_speed_ += command.at(3) * speed_change_;
                ROS_INFO("Linear speed set to %.2f", linear_speed_);
            }
            if (command.at(4) != 0)
            {
                angular_speed_ += command.at(4) * speed_change_;
                ROS_INFO("Angular speed set to %.2f", angular_speed_);
            }
        }
        else
        {
            ROS_INFO("Command not defined");
            is_message_ready = 0;
        }
    }
    return is_message_ready;
}

void KeyboardPublisher::Publish()
{
    chatter_pub_.publish(msg_);
}

void KeyboardPublisher::ResetMessage()
{
    msg_.linear.x = 0;
    msg_.linear.y = 0;
    msg_.linear.z = 0;
    msg_.angular.x = 0;
    msg_.angular.y = 0;
    msg_.angular.z = 0;
}

int KeyboardPublisher::Getch()
{
    int ch;
    struct termios oldt;
    struct termios newt;

    // Store old settings, and copy to new settings
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;

    // Make required changes and apply the settings
    newt.c_lflag &= ~(ICANON | ECHO);
    newt.c_iflag |= IGNBRK;
    newt.c_iflag &= ~(INLCR | ICRNL | IXON | IXOFF);
    newt.c_lflag &= ~(ICANON | ECHO | ECHOK | ECHOE | ECHONL | ISIG | IEXTEN);
    newt.c_cc[VMIN] = 1;
    newt.c_cc[VTIME] = 0;
    tcsetattr(fileno(stdin), TCSANOW, &newt);

    // Get the current character
    ch = getchar();

    // Reapply old settings
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

    return ch;
}