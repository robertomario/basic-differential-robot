#pragma once

#include <map>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

class KeyboardPublisher
{
public:
    KeyboardPublisher(ros::NodeHandle &n);

    ~KeyboardPublisher();

    int ParseKeyboard();

    void Publish();

private:
    void ResetMessage();
    int Getch();

    float linear_speed_;
    float angular_speed_;
    float speed_change_;
    ros::Publisher chatter_pub_;
    geometry_msgs::Twist msg_;
    std::map<char, std::array<int, 5>> reaction_map_;
};